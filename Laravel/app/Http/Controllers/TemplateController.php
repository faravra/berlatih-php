<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function page1(){
        return view('table');
    }
    public function page2(){
        return view('datatable');
    }
}
