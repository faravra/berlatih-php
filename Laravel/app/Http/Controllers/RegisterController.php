<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function page1(){
        return view('register');
    }
    public function page2(Request $minta){
        $firstName = $minta->namaAwal;
        $lastName = $minta->namaAkhir;
        return view('welcome', compact('firstName', 'lastName'));
    }
}
