<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="namaAwal"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="namaAkhir"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender"> Male<br>
        <input type="radio" name="gender"> Female<br><br>
        <label>Nationality:</label><br><br>
        <select name="negara">
            <option value="Indonesia">Indonesia</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Singapura">Singapura</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="35px" rows="12
        px"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>