@extends('adminlte.master')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Bordered Table</h3>
    </div>
            
    <div class="card-body">
        <a class="btn btn-primary mb-2" href="/cast/create">Create New Cast</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th>Setting</th>
                </tr>
            </thead>
            <tbody>
            @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td class="d-flex justify-content-center">
                    <a href="/cast/{{$value->id}}" class="btn btn-primary">Detail</a>
                    <a href="/cast/{{$value->id}}/edit" class="btn btn-success mx-2">Edit</a>
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </td>
            </tr>
            @empty
                <tr colspan="5">
                    <td>None</td>
                </tr>
            @endforelse

            </tbody>
        </table>
    </div>
</div>
@endsection