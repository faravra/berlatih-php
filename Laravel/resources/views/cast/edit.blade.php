@extends('adminlte.master')

@section('content')

<div class="card card-primary ml-3 mr-3 mt-3">
        <div class="card-header">
            <h3 class="card-title">Edit Cast</h3>
        </div>
    
        <form role="form" action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
            <div class="card-body">
                <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{$cast->nama}}" placeholder="Ketik Nama">
                    @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name="umur" value="{{$cast->umur}}" placeholder="Masukan Umur">
                    @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name="bio" value="{{$cast->bio}}">
                    @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
            </div>
                

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Save Edit</button>
                </div>
        </form>
    </div>

@endsection