@extends('adminlte.master');


@section('content')
<div class="m-4">
    <h5>Nama : {{$cast->nama}}</h5>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio : {{$cast->bio}}</p>
    <a href="/cast" class="btn btn-primary">Kembali</a>
</div>
@endsection
